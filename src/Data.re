open Belt;

module Examples = {
  let uploads: array(UploadParsing.upload) = UploadParsing.uploads;

  let kuba_elbow_id = "MzPpB1aBvy";
  let lukasz_elbow_id = "N0DhIoVoS0";
  let kuba_ud_id = "aa5YQvmapx";
  let lukasz_ud_id = "zfnpYVORJ2";

  let get_upload = (data: array(UploadParsing.upload), id) =>
    Array.keep(data, e => e.upload_id == id)
    ->MathSupport.RhArray.sortBy((x, y) => compare(x.time, y.time));

  let ke_data = get_upload(uploads, kuba_elbow_id);
  let le_data = get_upload(uploads, lukasz_elbow_id);
  let kud_data = get_upload(uploads, kuba_ud_id);
  let lud_data = get_upload(uploads, lukasz_ud_id);

  type uload_label = {
    label: string,
    up: array(UploadParsing.upload),
  };

  let empty = {
    open UploadParsing;
    let inner: ref(list(upload)) = ref([]);

    for (i in 1 to 400) {
      inner :=
        [
          {
            id: 1,
            upload_id: "empty",
            repetition: i / 80,
            x: i < 150 ? Random.float(9.0) -. 5.0 : 0.0,
            y: i < 150 ? Random.float(9.0) -. 5.0 : 0.0,
            z: i < 150 ? Random.float(9.0) -. 5.0 : 0.0,
            time: float_of_int(i) *. 82.0,
          },
          ...inner^,
        ];
    };
    (inner^)->List.reverse->List.toArray;
  };

  let tests = [
    {label: "empty", up: empty},
    {label: "ke_data", up: ke_data},
    {label: "le_data", up: le_data},
    {label: "kud_data", up: kud_data},
    {label: "lud_data", up: lud_data},
  ];

  let get_rep = (data: array(UploadParsing.upload), rep) =>
    Array.keep(data, e => e.repetition == rep);

  let ke_rep = get_rep(ke_data, 1);
  let le_rep = get_rep(le_data, 3);
  let kud_rep = get_rep(kud_data, 3);
  let lud_rep = get_rep(lud_data, 3);
};

module Test = {
  let model_rep = Examples.kud_rep;
  let test_reps = Examples.lud_data;
};
