module Error = {
  [@bs.deriving abstract]
  type t = {
    [@bs.as "type"]
    type_: string,
    code: string,
    message: string,
    row: int,
  };
};

module Meta = {
  [@bs.deriving abstract]
  type t = {
    delimiter: string,
    linebreak: string,
    aborted: bool,
    fields: array(string),
    truncated: bool,
  };
};



module Results = {
  [@bs.deriving abstract]
  type t = {
    data: array(array(string)),
    error: array(Error.t),
    meta: Meta.t,
  };
};

[@bs.val] [@bs.module "papaparse"]
external parse: string => Results.t = "parse";

let csv = Results.dataGet(parse(RawData.data))->Belt.Array.sliceToEnd(1);
