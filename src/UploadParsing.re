open Belt;
type key = {
  idx: int,
  label: string,
};

module Keys = {
  let id = {idx: 0, label: "id"};
  let upload_id = {idx: 1, label: "upload_id"};
  let repetition = {idx: 2, label: "repetition"};
  let x = {idx: 3, label: "x"};
  let y = {idx: 4, label: "y"};
  let z = {idx: 5, label: "z"};
  let time = {idx: 6, label: "time"};

  let all = [x, y, z, time];

  let byLabel = (label: string) =>
    List.keep(all, x => x.label == label) |> List.head; // # safe string programming
};

let getCol = (arr: array(array(string)), index) =>
  Array.map(arr, row => row |> Array.getUnsafe->index);

let table =
  <div>
    {Array.map(Csv.csv, row =>
       <tr>
         {Array.map(row, r => <td> {ReasonReact.string(r)} </td>)->React.array}
       </tr>
     )
     ->React.array}
  </div>;

type upload = {
  id: int,
  upload_id: string,
  repetition: int,
  x: float,
  y: float,
  z: float,
  time: float,
};

let csvRowToUpload = (row: array(string)) => {
  id: Array.getUnsafe(row, 0) |> int_of_string,
  upload_id: Array.getUnsafe(row, 1),
  repetition: Array.getUnsafe(row, 2) |> int_of_string,
  x: Array.getUnsafe(row, 3) |> float_of_string,
  y: Array.getUnsafe(row, 4) |> float_of_string,
  z: Array.getUnsafe(row, 5) |> float_of_string,
  time: Array.getUnsafe(row, 6) |> float_of_string,
};

let uploads: array(upload) = Array.map(Csv.csv, csvRowToUpload);

module Mapping = {


  let reps_in_ex = (up: array(upload)) => {
    up
    ->Belt.Array.map(x => x.repetition)
    ->Belt_SetInt.fromArray
    ->Belt_SetInt.toList
    ->List.sort(compare);
  };



  let uploadToJs = (u: upload) => {
    "repetition": u.repetition,
    "upload_id": u.upload_id,
    "x": u.x,
    "y": u.y,
    "z": u.z,
    "time": u.time,
  };
};
