module BArray = Belt.Array;
module BList = Belt.List;
module BOption = Belt.Option;

exception Broken(string);

module U = UploadParsing;

module RhArray = {
  let sortBy = (arr, by) =>
    arr->BList.fromArray->BList.sort(by) |> BList.toArray;
};

module RhFloat = {
  open Belt;
  let sum = (arr: array(float)) =>
    Array.reduce(arr, 0.0, (x: float, y: float) => x +. y);

  let sumList = (arr: list(float)) =>
    List.reduce(arr, 0.0, (x: float, y: float) => x +. y);

  let avg = (arr: array(float)) =>
    sum(arr) /. float_of_int(Array.size(arr));

  let avgList = (arr: list(float)) =>
    sumList(arr) /. float_of_int(List.size(arr));
};

module Dtw = {
  type matrix = array(array(float));

  let quotient_tresh = 0.0015;

  //TODO:bcm - add threshold
  // + window
  //
  let metric = (x: float, y: float) => {
    let diff = abs_float(x -. y);
    //Somehow this works best
    diff; //*. diff
  };

  let rec cost =
          (
            ~model: array(float),
            ~test: array(float),
            de: matrix,
            i: int,
            j: int,
          ) =>
    try (
      {
        if (de[i][j] == max_float) {
          let inon0 = i > 0;
          let jnon0 = j > 0;

          let prevs =
            BList.reduce(
              [
                inon0 && jnon0
                  ? cost(~model, ~test, de, i - 1, j - 1) : max_float,
                jnon0 ? cost(~model, ~test, de, i, j - 1) : max_float,
                inon0 ? cost(~model, ~test, de, i - 1, j) : max_float,
              ],
              max_float,
              (x, y) =>
              x < y ? x : y
            );

          de[i][j] =
            (prevs == max_float ? 0.0 : prevs) +. metric(model[i], test[j]);
        };
        de[i][j];
      }
    ) {
    | ex =>
      Js.log("Cost is broken");
      raise(ex);
    };

  let descendin_avg = (dtwsavg, ~tSize: int, beg: int, end_: int, tresh) => {
    let last: float = dtwsavg[end_];
    let slice = BArray.slice(dtwsavg, ~offset=beg, ~len=tSize - end_);
    BArray.some(slice, x => x -. last > tresh);
  };

  let dtw = (~model: array(U.upload), ~test: array(U.upload)) => {
    let test_size = Array.length(test);

    let xX = Array.make(test_size, 0.0);
    let yY = Array.make(test_size, 0.0);
    let zZ = Array.make(test_size, 0.0);
    let tT = Array.make(test_size, 0.0);
    let tSize = ref(0);

    let last_start_index = ref(0);
    let step_separators = ref([]);
    let dtwsavg = Array.make(test_size, 0.0);

    let model_size = Array.length(model);
    let modelx = BArray.map(model, u => u.x);
    let modely = BArray.map(model, u => u.y);
    let modelz = BArray.map(model, u => u.z);

    let dX = Array.make_matrix(model_size, test_size, max_float);
    let dY = Array.make_matrix(model_size, test_size, max_float);
    let dZ = Array.make_matrix(model_size, test_size, max_float);

    let resetArr = () => {
      BArray.forEach(dX, row => Array.fill(row, 0, test_size, max_float));
      BArray.forEach(dY, row => Array.fill(row, 0, test_size, max_float));
      BArray.forEach(dZ, row => Array.fill(row, 0, test_size, max_float));
    };

    BArray.forEachWithIndex(
      test,
      (i, m) => {
        xX[tSize^] = m.x;

        yY[tSize^] = m.y;

        zZ[tSize^] = m.z;

        tT[tSize^] = m.time;
        tSize := tSize^ + 1;

        if (i - last_start_index^ > 1) {
          let costX =
            xX
            ->BArray.slice(
                ~offset=last_start_index^,
                ~len=tSize^ - last_start_index^,
              )
            ->(
                test =>
                  cost(
                    ~model=modelx,
                    ~test,
                    dX,
                    model_size - 1,
                    i - last_start_index^,
                  )
              );

          let costY =
            yY
            ->BArray.slice(
                ~offset=last_start_index^,
                ~len=tSize^ - last_start_index^,
              )
            ->(
                test =>
                  cost(
                    ~model=modely,
                    ~test,
                    dY,
                    model_size - 1,
                    i - last_start_index^,
                  )
              );

          let costZ =
            zZ
            ->BArray.slice(
                ~offset=last_start_index^,
                ~len=tSize^ - last_start_index^,
              )
            ->(
                test =>
                  cost(
                    ~model=modelz,
                    ~test,
                    dZ,
                    model_size - 1,
                    i - last_start_index^,
                  )
              );

          let avg = 1.0 /. RhFloat.avg([|costX, costY, costZ|]);
          /* let avg = 1.0 /. costZ; */

          dtwsavg[tSize^ - 1] = avg;
          if (descendin_avg(
                dtwsavg,
                ~tSize=tSize^,
                max(i - 5, last_start_index^),
                i,
                quotient_tresh,
              )) {
            last_start_index := i;
            step_separators := step_separators^ @ [i];

            try (
              {
                resetArr();
              }
            ) {
            | ex =>
              Js.log("fill is broken");
              raise(ex);
            };
          };

          if (m.time -. test[last_start_index^].time > 3.0 *. 1000.0) {
            last_start_index := i;
            resetArr();
          };
        } else {
          dtwsavg[tSize^ - 1] = 0.0;
        };
      },
    );

    //moving average:
    BArray.forEachWithIndex(dtwsavg, (ind, _) =>
      dtwsavg[ind] = (
        switch (ind) {
        | 0 => (dtwsavg[0] +. dtwsavg[1]) /. 2.0
        | i when i < Array.length(dtwsavg) - 1 =>
          (dtwsavg[i - 1] +. dtwsavg[i] +. dtwsavg[i + 1]) /. 3.0
        | maxInd => (dtwsavg[maxInd - 1] +. dtwsavg[maxInd]) /. 2.0
        }
      )
    );

    dtwsavg;
  };

  let peaks = (dtwsavg: array(float)) => {
    let mean = RhFloat.avg(dtwsavg);
    BArray.map(dtwsavg, x => x < mean ? 0.0 : x);
  };

  let argmax = (arr: array(float)) => {
    arr
    ->BArray.reduceWithIndex(
        ((-1.0), (-1)), ((maxVal, maxInd), currentVal, currentInd) =>
        if (maxVal < currentVal) {
          (currentVal, currentInd);
        } else {
          (maxVal, maxInd);
        }
      )
    ->(
        fun
        | (_, ind) => ind
      );
  };

  let peak_inds = (avgs: array(float)) => {
    let peaks = ref([]);
    let startPoint = ref(None);

    let seeing_floor = ref(avgs[0] <= 0.0);

    BArray.forEachWithIndex(avgs, (i, value) =>
      if (value > 0.0) {
        if (seeing_floor^) {
          seeing_floor := false;
          startPoint := Some(i);
        };
      } else {
        if (! seeing_floor^ && startPoint^ != None) {
          let start = BOption.getExn(startPoint^);
          let len = i - start;

          let peakIndex =
            start + argmax(BArray.slice(avgs, ~offset=start, ~len));
          peaks := peaks^ @ [peakIndex];
        };
        seeing_floor := true;
      }
    );
    peaks^;
  };

  let dtw_metric = (~model: array(U.upload), ~test: array(U.upload)) =>
    if (Array.length(test) < 10) {
      (0.0, []);
    } else {
      let avgs = dtw(~model, ~test);

      let dtw_peaks = peaks(avgs);

      let dtw_peak_inds = peak_inds(dtw_peaks);

      let dtw_scodres = BList.map(dtw_peak_inds, ind => avgs[ind]);

      (RhFloat.avgList(dtw_scodres), dtw_peak_inds);
    };

  let repetitions = (~model: array(U.upload), ~test: array(U.upload)) =>
    if (Array.length(test) < 10) {
      0;
    } else {
      dtw(~model, ~test)->peaks->peak_inds->List.length;
    };
};
