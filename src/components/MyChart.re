open BsRecharts;

let component = ReasonReact.statelessComponent("SomeComponent");

let make = //FIXME - won't work with records - make it type-safe
    (
      ~data,
      ~dataKey: string,
      ~labelKey: string,
      ~original_reps=[||],
      ~detected_reps=[||],
      ~title=?,
      _children,
    ) => {
  ...component,
  render: _self => {
    <ResponsiveContainer height={Px(600.0)} width={Px(800.0)}>
      <LineChart margin={"top": 0, "right": 0, "bottom": 0, "left": 0} data>
        <Line
          name={Belt_Option.getWithDefault(title, "")}
          dataKey
          stroke="#2078b4"
          animationDuration=0
        />
        <XAxis
          dataKey=labelKey
          _type=`number
          domain=[|
            /* dataMin => Js.Math.floor(dataMin), */
            /* dataMax => Js.Math.ceil(dataMax), */
            "auto",
            "auto",
          |]
        />
        <YAxis
          domain=[|
            /* dataMin => Js.Math.floor(dataMin), */
            /* dataMax => Js.Math.ceil(dataMax), */
            "auto",
            "auto",
          |]
        />
        {Belt.Array.map(original_reps, x =>
           <ReferenceLine x className="original" stroke="green" />
         )
         ->React.array}
        {Belt.Array.map(detected_reps, x => <ReferenceLine x stroke="red" />)
         ->React.array}
        <Tooltip />
        <Legend align=`left iconType=`circle />
      </LineChart>
    </ResponsiveContainer>;
  },
};
