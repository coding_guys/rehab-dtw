// Generated by BUCKLESCRIPT VERSION 5.0.4, PLEASE EDIT WITH CARE
'use strict';

var Curry = require("bs-platform/lib/js/curry.js");
var React = require("react");
var Caml_obj = require("bs-platform/lib/js/caml_obj.js");
var Belt_List = require("bs-platform/lib/js/belt_List.js");
var Belt_Array = require("bs-platform/lib/js/belt_Array.js");
var Belt_SetInt = require("bs-platform/lib/js/belt_SetInt.js");
var Caml_format = require("bs-platform/lib/js/caml_format.js");
var Csv$ReactTemplate = require("./Csv.bs.js");

var x = /* record */[
  /* idx */3,
  /* label */"x"
];

var y = /* record */[
  /* idx */4,
  /* label */"y"
];

var z = /* record */[
  /* idx */5,
  /* label */"z"
];

var time = /* record */[
  /* idx */6,
  /* label */"time"
];

var all_001 = /* :: */[
  y,
  /* :: */[
    z,
    /* :: */[
      time,
      /* [] */0
    ]
  ]
];

var all = /* :: */[
  x,
  all_001
];

function byLabel(label) {
  return Belt_List.head(Belt_List.keep(all, (function (x) {
                    return x[/* label */1] === label;
                  })));
}

var Keys_000 = /* id : record */[
  /* idx */0,
  /* label */"id"
];

var Keys_001 = /* upload_id : record */[
  /* idx */1,
  /* label */"upload_id"
];

var Keys_002 = /* repetition : record */[
  /* idx */2,
  /* label */"repetition"
];

var Keys = /* module */[
  Keys_000,
  Keys_001,
  Keys_002,
  /* x */x,
  /* y */y,
  /* z */z,
  /* time */time,
  /* all */all,
  /* byLabel */byLabel
];

function getCol(arr, index) {
  return Belt_Array.map(arr, (function (row) {
                return Curry._2(index, (function (prim, prim$1) {
                              return prim[prim$1];
                            }), row);
              }));
}

var table = React.createElement("div", undefined, Belt_Array.map(Csv$ReactTemplate.csv, (function (row) {
            return React.createElement("tr", undefined, Belt_Array.map(row, (function (r) {
                              return React.createElement("td", undefined, r);
                            })));
          })));

function csvRowToUpload(row) {
  return /* record */[
          /* id */Caml_format.caml_int_of_string(row[0]),
          /* upload_id */row[1],
          /* repetition */Caml_format.caml_int_of_string(row[2]),
          /* x */Caml_format.caml_float_of_string(row[3]),
          /* y */Caml_format.caml_float_of_string(row[4]),
          /* z */Caml_format.caml_float_of_string(row[5]),
          /* time */Caml_format.caml_float_of_string(row[6])
        ];
}

var uploads = Belt_Array.map(Csv$ReactTemplate.csv, csvRowToUpload);

function reps_in_ex(up) {
  return Belt_List.sort(Belt_SetInt.toList(Belt_SetInt.fromArray(Belt_Array.map(up, (function (x) {
                            return x[/* repetition */2];
                          })))), Caml_obj.caml_compare);
}

function uploadToJs(u) {
  return {
          repetition: u[/* repetition */2],
          upload_id: u[/* upload_id */1],
          x: u[/* x */3],
          y: u[/* y */4],
          z: u[/* z */5],
          time: u[/* time */6]
        };
}

var Mapping = /* module */[
  /* reps_in_ex */reps_in_ex,
  /* uploadToJs */uploadToJs
];

exports.Keys = Keys;
exports.getCol = getCol;
exports.table = table;
exports.csvRowToUpload = csvRowToUpload;
exports.uploads = uploads;
exports.Mapping = Mapping;
/* table Not a pure module */
