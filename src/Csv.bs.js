// Generated by BUCKLESCRIPT VERSION 5.0.4, PLEASE EDIT WITH CARE
'use strict';

var Papaparse = require("papaparse");
var Belt_Array = require("bs-platform/lib/js/belt_Array.js");
var RawData$ReactTemplate = require("./RawData.bs.js");

var $$Error = /* module */[];

var Meta = /* module */[];

var Results = /* module */[];

var csv = Belt_Array.sliceToEnd(Papaparse.parse(RawData$ReactTemplate.data).data, 1);

exports.$$Error = $$Error;
exports.Meta = Meta;
exports.Results = Results;
exports.csv = csv;
/* csv Not a pure module */
