open Belt;

module PrepData = {
  let to_plot = (~dtw: array(float), ~test: array(UploadParsing.upload)) => {
    let beginTime = Js.Date.now();

    let times = test->Belt_Array.map(x => x.time);

    let res =
      Array.zip(dtw, times)
      ->Array.map(dtw => {"dtw": fst(dtw), "time": snd(dtw)});

    let endTime = Js.Date.now();
    Js.log(endTime -. beginTime);
    res;
  };
};

module Indeksik = {
  type action =
    | UpdateModel(string)
    | UpdateRep(int)
    | UpdateTest(string);

  type state = {
    model_label: string,
    model_rep: int,
    test_label: string,
    test_data: array(UploadParsing.upload),
    model_data: array(UploadParsing.upload),
  };

  let upByKey = (ala: string) => {
    (
      Belt.List.getBy(Data.Examples.tests, x => x.label == ala)
      |> Belt.Option.getExn
    ).
      up;
  };
  let plot_data_provider = (~model, ~test) =>
    MathSupport.Dtw.dtw(~model, ~test);

  let component = ReasonReact.reducerComponent("Example");

  let make = _ => {
    ...component,
    initialState: _ => {
      model_label: "ke_data",
      model_rep: 1,
      test_label: "le_data",
      model_data: Data.Examples.ke_data,
      test_data: Data.Examples.le_data,
    },
    reducer: (action, state) =>
      switch (action) {
      | UpdateModel(str) =>
        ReasonReact.Update(
          {
            let maxInModel =
              upByKey(str)
              ->UploadParsing.Mapping.reps_in_ex
              ->Belt.List.reduce(0, (x: int, y: int) => x < y ? y : x);

            {
              ...state,
              model_label: str,
              model_data: upByKey(str),
              model_rep: min(maxInModel, state.model_rep),
            };
          },
        )
      | UpdateRep(str) =>
        ReasonReact.Update(
          {
            {...state, model_rep: str};
          },
        )
      | UpdateTest(str) =>
        ReasonReact.Update({
          ...state,
          test_label: str,
          test_data: upByKey(str),
        })
      },

    render: self => {
      let model_rep =
        Belt.Array.keep(self.state.model_data, x =>
          x.repetition == self.state.model_rep
        );

      let dtw_plot_data =
        plot_data_provider(~model=model_rep, ~test=self.state.test_data);

      let dtw_ploat_peaks = MathSupport.Dtw.peaks(dtw_plot_data);

      let (metric, detected_reps_ind) =
        try (
          MathSupport.Dtw.dtw_metric(
            ~model=model_rep,
            ~test=self.state.test_data,
          )
        ) {
        | ex =>
          Js.log(self.state);
          raise(ex);
        };

      let detected_reps =
        List.map(detected_reps_ind, ind =>
          Js.Float.toString(
            Belt.Option.getExn(self.state.test_data[ind]).time,
          )
        )
        ->Belt.List.toArray;

      let original_reps =
        Array.reduce(self.state.test_data, [], (acc, current) =>
          switch (acc) {
          | [] => [current]
          | [head, ..._tail] when head.repetition != current.repetition =>
            [current] @ acc
          | _ => acc
          }
        ) -> List.reverse -> List.tailExn -> List.map( x => Js.Float.toString(x.time))  -> List.toArray; 

      <>
        {ReasonReact.string("Model ")}
        <select
          value={self.state.model_label}
          onChange={(x: ReactEvent.Form.t) => {
            ReactEvent.Form.preventDefault(x);
            let value = ReactEvent.Form.target(x)##value;
            self.send(UpdateModel(value));
          }}>
          {Array.map(Data.Examples.tests |> List.toArray, x =>
             <option value={x.label}> {ReasonReact.string(x.label)} </option>
           )
           ->React.array}
        </select>
        <select
          value={string_of_int(self.state.model_rep)}
          onChange={(x: ReactEvent.Form.t) => {
            ReactEvent.Form.preventDefault(x);
            let value = ReactEvent.Form.target(x)##value;
            self.send(UpdateRep(int_of_string(value)));
          }}>
          {Belt.Array.map(
             upByKey(self.state.model_label)
             ->UploadParsing.Mapping.reps_in_ex
             ->List.toArray,
             x =>
             <option value={string_of_int(x)}>
               {ReasonReact.string(string_of_int(x))}
             </option>
           )
           ->React.array}
        </select>
        {ReasonReact.string("Test ")}
        <select
          value={self.state.test_label}
          onChange={(x: ReactEvent.Form.t) => {
            ReactEvent.Form.preventDefault(x);
            let value = ReactEvent.Form.target(x)##value;
            self.send(UpdateTest(value));
          }}>
          {Array.map(Data.Examples.tests |> List.toArray, x =>
             <option value={x.label}> {ReasonReact.string(x.label)} </option>
           )
           ->React.array}
        </select>
        {ReasonReact.string(
           " Metric: "
           ++ Js.Float.toString(metric)
           ++ " Rep: "
           ++ string_of_int(List.length(detected_reps_ind))
          ++ " True rep: "
          ++ string_of_int(Array.length(original_reps))
         )}
        <div style=(ReactDOMRe.Style.make(~display="flex", ()))>
        <MyChart
          data={PrepData.to_plot(
            ~dtw=dtw_plot_data,
            ~test=self.state.test_data,
          )}
          dataKey="dtw"
          labelKey="time"
          original_reps
          detected_reps
        />
        <MyChart
          title ="peaks"
          data={PrepData.to_plot(
            ~dtw=dtw_ploat_peaks,
            ~test=self.state.test_data,
          )}
          dataKey="dtw"
          labelKey="time"
          original_reps
          detected_reps
        />
        </div>
        <div style=(ReactDOMRe.Style.make(~display="flex", ()))>
        <MyChart
          title="test x"
          data={Array.map(self.state.test_data, UploadParsing.Mapping.uploadToJs)}
          dataKey="x"
          labelKey="time"
          original_reps
          detected_reps
        />
        <MyChart
          title="test y"
          data={Array.map(self.state.test_data, UploadParsing.Mapping.uploadToJs)}
          dataKey="y"
          labelKey="time"
          original_reps
          detected_reps
        />
        </div>
        <MyChart
          title="test z"
          data={Array.map(self.state.test_data, UploadParsing.Mapping.uploadToJs)}
          dataKey="z"
          labelKey="time"
          original_reps
          detected_reps
        />
        /* <UploadTable data /> */
      </>;
    },
  };
};

ReactDOMRe.renderToElementWithId(<Indeksik />, "root");
