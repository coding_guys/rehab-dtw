open Belt;

let component = ReasonReact.statelessComponent("Upload Table");

let makeRow = (row: UploadParsing.upload) => {
  <tr>
    <td> {ReasonReact.string(row.repetition |> string_of_int)} </td>
    <td> {ReasonReact.string(row.x |> Js.Float.toString)} </td>
    <td> {ReasonReact.string(row.y |> Js.Float.toString)} </td>
    <td> {ReasonReact.string(row.z |> Js.Float.toString)} </td>
    <td> {ReasonReact.string(row.time |> Js.Float.toString)} </td>
  </tr>;
};

let make = (~data, _children) => {
  ...component,
  render: _self =>
    <table>
      {
        Js.log("Size: " ++ string_of_int(Array.size(data)));
        Array.map(data, makeRow)->React.array;
      }
    </table>,
};
